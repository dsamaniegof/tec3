import "./Alumno.css";
import { useState } from "react";

export default function StudentForm(props) {
 const {setStudentList} = props;

  const [student, setStudent] = useState({
    name: "",
    studentId: "",
    semester: "",
    average: "",
  });

  //Student Handler.
  const handleStudent= (e, key) => setStudent({ ...student, [key]: e.target.value });

  //Appends new student to StudentList Hook.
  function createNewStudent(event) {
    event.preventDefault();
    console.log(student);

    setStudentList((oldArray) => [...oldArray, student]);
  }

  return (
    <form onSubmit={createNewStudent}>
      <div className="alumno-item">
        <div className="alumno-item__pr">
          <input
            name="nombre"
            type="text"
            onChange={(e) => handleStudent(e, "name")}
            value={student.name}
          ></input>
        </div>
        <div>
          <input
            name="studentId"
            type="text"
            onChange={(e) => handleStudent(e, "studentId")}
            value={student.studentId}
          ></input>
        </div>
        <div className="alumno-item__p ">
          <input
            name="promedio"
            type="text"
            onChange={(e) => handleStudent(e, "average")}
            value={student.average}
          ></input>
        </div>
        <div>
          <input
            name="semestre"
            type="text"
            onChange={(e) => handleStudent(e, "semester")}
            value={student.semester}
          ></input>
        </div>
        <button type="submit">Nuevo</button>
      </div>
    </form>
  );
}
