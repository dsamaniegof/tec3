import "./App.css";
import Alumno from "./Alumno.js";
import StudentForm from "./StudentForm.js";
import { useState } from "react";

const defaultStudentList = [
  {
    name: "Daniel Samaniego",
    studentId: "900456",
    average: 70,
    semester: 5,
  },
  {
    name: "Rebeca Ramirez",
    studentId: "900500",
    average: 71,
    semester: 7,
  },
  {
    name: "Antonio Romero",
    studentId: "900800",
    average: 71,
    semester: 7,
  },
];

function App() {
  const [studentList, setStudentList] = useState(defaultStudentList);

  console.log(studentList);

  return (
    <main className="App">
      <StudentForm setStudentList={setStudentList} />
      {studentList.map((student, index) => (
        <Alumno key={index} studentInfo={student} />
      ))}
    </main>
  );
}

export default App;
