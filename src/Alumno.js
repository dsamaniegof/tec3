import {useState} from 'react';
import './Alumno.css';
function Alumno(props)
{   
    const {name, studentId, average, semester} = props.studentInfo;

    const [semestre1, setSemestre1] = useState(Number(semester));

    const calcularSemestre = () => {
      if (average >= 70) 
        setSemestre1(semestre1 + 1);
    }

    return (
        <div className="alumno-item">
            <div className="alumno-item__pr">{name}</div>
            <div>{studentId}</div>
            <div className="alumno-item__p ">{average}</div>
            <div>{semestre1}</div>
            <button onClick={calcularSemestre}>Calcular</button>
        </div>
    )
}

export default Alumno;